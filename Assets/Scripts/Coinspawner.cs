using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Coinspawner : MonoBehaviour
{
    public GameObject coinPrefab;
    public GameObject walls;
    public GameObject green;
    public GameObject red;
    public GameObject pwerup;
    public GameObject player;
    public GameObject cherry;
    private int randomgreen;
    private int randomred;
    private float timercherry;
    private Vector3 spawn;
    private int randomcherry;

    void Start()
    {
        
        timercherry = Random.Range(10,30);
        Instantiate(walls, walls.transform.position,walls.transform.rotation );

        Instantiate(pwerup, new Vector3(-21.2f, 1.43f, 28.9f), Quaternion.identity);
        Instantiate(pwerup, new Vector3(33.3f, 1.43f, 28.9f), Quaternion.identity);
        Instantiate(pwerup, new Vector3(33.3f, 1.43f, -9.1f), Quaternion.identity);
        Instantiate(pwerup, new Vector3(-21.2f, 1.43f, -9.1f), Quaternion.identity);

        Instantiate(coinPrefab,coinPrefab.transform.position, Quaternion.identity);

        Instantiate(player, player.transform.position, Quaternion.identity);

        randomred = Random.Range(1, 4);
        randomgreen = Random.Range(1, 4);
        if(!GameObject.Find("Red(Clone)"))
        {
            Instantiate(red, new Vector3(5.8f, 0, 25.76f), Quaternion.identity);
            /*switch (randomred)
            {
                case 1:
                    Instantiate(red, new Vector3(28.5f, -0.54f, -17.6f), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(red, new Vector3(31.9f, -0.54f, 12.2f), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(red, new Vector3(0.6f, -0.54f, -6.5f), Quaternion.identity);
                    break;
            }*/
        }
        
        if(!GameObject.Find("Green(Clone)"))
        {
            Instantiate(green, new Vector3(9.69f, 0, 8.26f), Quaternion.identity);
            /*switch (randomgreen)
        {
            case 1:
                Instantiate(green, new Vector3(-19.3f, -0.54f, 12.6f), Quaternion.identity);
                break;
            case 2:
                Instantiate(green, new Vector3(-19.3f, -0.54f, -19f), Quaternion.identity);
                break;
            case 3:
                Instantiate(green, new Vector3(32.4f, -0.54f, 19.5f), Quaternion.identity);
                break;
        }*/
        }
    }
    private void Update()
    {
        randomred = Random.Range(1, 4);
        randomgreen = Random.Range(1, 4);
        if (!GameObject.Find("Red(Clone)"))
        {
            Instantiate(red, new Vector3(5.8f, 0, 25.76f), Quaternion.identity);
            /*switch (randomred)
            {
                case 1:
                    Instantiate(red, new Vector3(28.5f, -0.54f, -17.6f), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(red, new Vector3(31.9f, -0.54f, 12.2f), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(red, new Vector3(0.6f, -0.54f, -6.5f), Quaternion.identity);
                    break;
            }*/
        }
        if (!GameObject.Find("Green(Clone)"))
            {
                Instantiate(green, new Vector3(0.7f, 0, 9f), Quaternion.identity);
                    /*switch (randomgreen)
                {
                    case 1:
                        Instantiate(green, new Vector3(-19.3f, -0.54f, 12.6f), Quaternion.identity);
                        break;
                    case 2:
                        Instantiate(green, new Vector3(-18.1f, -0.54f, -19f), Quaternion.identity);
                        break;
                    case 3:
                        Instantiate(green, new Vector3(28.7f, -0.54f, 19.5f), Quaternion.identity);
                        break;
                }*/
            }
        

        if (!GameObject.Find("Cherry(Clone)"))
        {
            randomcherry = Random.Range(1, 5);
            timercherry -= Time.deltaTime;
            if(timercherry < 0)
            {
                timercherry = Random.Range(10, 30);
                switch (randomcherry)
                {
                    case 1:
                        spawn = new Vector3(6.4f, -0.4f, 25.3f);
                        break;
                    case 2:
                        spawn = new Vector3(5.6f, -0.4f, 2.15f);
                        break;
                    case 3:
                        spawn = new Vector3(20.8f, -0.4f, -20.6f);
                        break;
                    case 4:
                        spawn = new Vector3(-9.6f, -0.4f, -20.6f);
                        break;
                }
                Instantiate(cherry, spawn , cherry.transform.rotation);
            } 
        }

    }
}
