using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;
using Random = UnityEngine.Random;

public class Redcontroller : MonoBehaviour
{
    private Rigidbody rb;
    private Transform tr;
    private NavMeshAgent agent;
    public Material normalmat;
    public Material fearmat;
    public Material deathmat;
    private GameObject player;
    private PlayerControl playerControl;
    private string state;
    private float randomX;
    private float randomZ;
    private float statetimer;
    public bool death;
    private Collider col;
    private float deathtimer;


    // Start is called before the first frame update
    void Start()
    {
        statetimer = 20f;
        deathtimer = 20f;
        state = "wonder";
        player = GameObject.Find("Sphere(Clone)");
        death = false;
        col=GetComponent<Collider>();
        if (player!=null)
        {
            tr = player.transform;
            playerControl = player.GetComponent<PlayerControl>();
        }
            

        rb = GetComponent<Rigidbody>();
        
        agent=GetComponent<NavMeshAgent>();

        randomZ = Random.Range(-25f, 36.5f);
        randomX = Random.Range(-23.5f, 36.5f);
    }

    // Update is called once per frame
    void Update()
    {
        statetimer -= Time.deltaTime;
        if (statetimer < 0)
        {
            state = "chase";
        }
        if(player!=null)
        tr = player.transform;

        if (!death)
        {
            if (playerControl != null && playerControl.poweractive == true)
            {
                gameObject.GetComponent<SkinnedMeshRenderer>().material = fearmat;
                Vector3 directionToFlee = transform.position - tr.position;
                agent.destination = transform.position + directionToFlee;
            }

            if (playerControl != null && playerControl.poweractive == false)
            {
                Vector3 wonder = new Vector3(randomX, -0.6189656f, randomZ);
                gameObject.GetComponent<SkinnedMeshRenderer>().material = normalmat;
                if (player != null && state == "chase")
                {
                    agent.destination = tr.position;
                }
                else if (player != null && state == "wonder")
                {
                    if (tr.position==wonder)
                    {
                        randomZ = Random.Range(-25f, 36.5f);
                        randomX = Random.Range(-23.5f, 36.5f);
                    }
                    else
                    {
                        agent.destination = new Vector3(randomX, 0, randomZ);
                    }
                }
            }
        }

        if (death)
        {
            Vector3 dest= new Vector3(2.43f, -0.6189656f, 8.26f); 
            agent.destination = dest;
            col.isTrigger = true;
            gameObject.GetComponent<SkinnedMeshRenderer>().material = deathmat;
            if (transform.position==dest)
            {
                deathtimer -= Time.deltaTime;
                if (deathtimer < 0)
                {
                    deathtimer = 20;
                    death = false;
                }
                col.isTrigger = false;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.Equals("Wall1"))
        {
            transform.position = new Vector3(7.5f, 0f, -19.6f);
        }
        else if (collision.gameObject.name.Equals("Wall2"))
        {
            transform.position = new Vector3(6.9f, 0f, 19.7f);
        }
        else if (collision.gameObject.name.Equals("Sphere(Clone)"))
        { 
            if(playerControl.poweractive == true)
            {
                /*Destroy(gameObject);*/
                death = true;
                
            }
        }
        else if (collision.gameObject.name.Equals("Green(Clone)"))
        {
            col.isTrigger = true;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("Green(Clone)"))
        {
            col.isTrigger = false;
        }
    }
}
